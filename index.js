const WebSocket = require('ws');
const connect = require('connect');
const serveStatic = require('serve-static');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
let db = low(adapter);

let http_port = 8000;

if (process.env.DEVELOP) {
  http_port = 8001;
  const adapter = new FileSync('db_dev.json');
  db = low(adapter);
}

const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');

const server = http.createServer(function(req, res) {
  console.log(`${req.method} ${req.url}`);

  // parse URL
  const parsedUrl = url.parse(req.url);
  // extract URL path
  let pathname = `./static/${parsedUrl.pathname}`;
  // based on the URL path, extract the file extention. e.g. .js, .doc, ...
  const ext = path.parse(pathname).ext || '.html';
  // maps file extention to MIME typere
  const map = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword',
  };

  fs.exists(pathname, function(exist) {
    if (!exist) {
      // if the file is not found, return 404
      res.statusCode = 404;
      res.end(`File ${pathname} not found!`);
      return;
    }

    // if is a directory search for index file matching the extention
    if (fs.statSync(pathname).isDirectory()) pathname += '/index' + ext;

    // read file from file system
    fs.readFile(pathname, function(err, data) {
      if (err) {
        res.statusCode = 500;
        res.end(`Error getting the file: ${err}.`);
      } else {
        // if the file is found, set Content-type and send data
        res.setHeader('Content-type', map[ext] || 'text/plain' );
        res.end(data);
      }
    });
  });
}).listen(http_port);

const wss = new WebSocket.Server({server});

db.defaults({msgs: [], count: 0})
    .write();

function validateData(data, ip) {
  let latest = db
      .get('msgs')
      .filter({user_id: ip})
      .sortBy('time')
      .reverse()
      .take(1)
      .value();

  if (latest.length == 1) {
    latest = latest[0];
  }
  if (latest && ((new Date()).getTime() - latest.time) < 3000) {
    return null;
  }
  try {
    data = JSON.parse(data);
  } catch (e) {
    return null;
  }

  if (data.topic != '@tg') {
    return null;
  }

  const valid_data = {};
  if (!data.text) {
    return null;
  }
  data.text = data.text.replace(/^\s+|\s+$/g, '');
  if (!data.text) {
    return null;
  }
  const user_name = data.user_name ? data.user_name.slice(0, 100) : 'Anonymous';
  valid_data.user_name = user_name;
  valid_data.text = data.text.slice(0, 155);
  valid_data.time = (new Date()).getTime();
  db.update('count', (n) => n + 1).write();
  valid_data.msg_id = db.get('count').value();
  valid_data.user_id = ip;

  db.get('msgs').push(valid_data).write();
  return JSON.stringify(valid_data);
}

function broadcast(msg) {
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(msg);
    }
  });
}

function sendLatest(ws, num, ip) {
  const handshake = {
    handshake: 1,
    user_id: ip,
  };
  ws.send(JSON.stringify(handshake));
  const msgs = db
      .get('msgs')
      .sortBy('time')
      .reverse()
      .take(num)
      .sortBy('time')
      .value();
  for (const msg of msgs) {
    ws.send(JSON.stringify(msg));
  }
}

wss.on('connection', function connection(ws, req) {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || ws._socket.remoteAddress;
  sendLatest(ws, 100, ip);
  console.log('Clients connected:', wss.clients.size);
  ws.on('message', function incoming(data) {
    const validated_data = validateData(data, ip);
    if (validated_data) {
      broadcast(validated_data);
    }
  });
});

