// planned top bar render is [[ topic1 ]][ topic2 ][ topic3 ][ topic4 ]...[+]
//
// msg data is { topic, [responses (including other topics)], user_id, msg_id, msg }
//
// msg render is
//
// |================================================|
// | date/time id            [ responses...        ]| // overflow x
// |------------------------------------------------|
// | >>response1, >>response2, ...                  |
// | text (with wrap, max y, ability to reveal more)|
// | ...                                            |
// | ...                                            |
// |================================================|
//
// if response is directed as you, border highlighted red on the left
// links are clickable
//
// clicking a response scrolls up to the response
// down arrow pops up to scroll down

const max_topics = 10;
const max_num_lines = 4;
const max_responses = 5;
let g_is_scrolling = true;
let user_name = 'Anonymous';

// topic : msg[]
const msgs = { };

function elem(elem_type, clsname) {
  const d = document.createElement(elem_type);
  d.classList.toggle(clsname);
  return d;
}

function getDateString(d) {
  d = new Date(d);
  const minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes();
  const hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours();
  const ampm = d.getHours() >= 12 ? 'pm' : 'am';
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  return days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+' '+d.getFullYear()+' '+hours+':'+minutes+ampm;
}

function id_click(id) {
  const f = function() {
    const input = document.getElementById('input');
    input.value += '>>' + id + ' ';
    input.focus();
    input.setSelectionRange(input.value.length, input.value.length);
  };
  return f;
}

let my_user_id = null;
const my_msgs = [];
let tab_focused = true;
let unread_msgs = 0;
let max_scroll = 0;
const latest_msg_div = null;

function renderMessage(msg) {
  msg = JSON.parse(msg);
  if (msg.handshake && msg.user_id) {
    my_user_id = msg.user_id;
    return;
  }
  if (!tab_focused) {
    unread_msgs++;
    document.title = '('+unread_msgs+') chat.fit';
  }
  const msgs_div = document.getElementById('msgs');

  const msg_div = elem('div', 'msg');

  // Create Header
  const msg_header = elem('div', 'msg_header');
  const msg_user_name = elem('span', 'msg_user_name');
  const msg_id = elem('span', 'msg_id');
  const msg_date = elem('span', 'msg_date');
  msg_user_name.textContent = msg.user_name ? msg.user_name : 'Anonymous';
  msg_date.textContent = getDateString(msg.time);
  msg_id.textContent = msg.msg_id;
  if (msg.user_id == my_user_id) {
    my_msgs.push(msg.msg_id);
    msg_div.classList.toggle('me');
  }
  msg_id.addEventListener('click', id_click(msg.msg_id));
  msg_header.appendChild(msg_user_name);
  msg_header.appendChild(msg_date);
  msg_header.appendChild(msg_id);
  msg_div.appendChild(msg_header);

  // Create body
  const msg_body = elem('div', 'msg_body');
  const re = />>[0-9]+/g;
  const replies = msg.text.match(re);
  let highlight = false;
  if (replies) {
    for (const reply of replies) {
      const num = parseInt(reply.slice(2));
      if (my_msgs.indexOf(num) > -1) {
        highlight = true;
      }
    }
  }

  msg_body.textContent = msg.text;

  // Parse links out of the body. This is probably dangerous
  const new_html = msg_body.innerHTML.replace(
      /([^\S]|^)(((https?\:\/\/)|(www\.))(\S+))/gi,
      function(match, space, url) {
        let hyperlink = url;
        if (!hyperlink.match('^https?:\/\/')) {
          hyperlink = 'http://' + hyperlink;
        }
        return space + '<a target="_blank" href="' + hyperlink + '">' + url + '</a>';
      }
  );
  msg_body.innerHTML = new_html;

  msg_div.appendChild(msg_body);
  if (highlight) {
    msg_div.classList.toggle('highlight');
  }

  msgs_div.appendChild(msg_div);

  if (g_is_scrolling) {
    scrollToBottom();
  }
}

function setTheme() {
  const head = document.head;
  const link = document.createElement('link');

  link.type = 'text/css';
  link.rel = 'stylesheet';
  link.href = 'imessage.css';

  head.appendChild(link);
  // TODO figure out a better way to do this
  setTimeout(function() {
    scrollToBottom();
  }, 300);
}

// Soft validation, everything is done on the server as well
let last_msg_time = new Date(0);
function validateMessage(input) {
  let text = input.value;
  text = text.replace(/^\s+|\s+$/g, '');
  if (text == '/ios' || text == '/sneak' || text == '/stealth') {
    setTheme();
    input.value = '';
    return;
  }
  if (text == '/name') {
    user_name = prompt('Name? Leave empty for \'Anonymous\'');
    user_name = user_name ? user_name : 'Anonymous';
    localStorage.setItem('name', user_name);
    input.value = '';
    return;
  }
  if (text.length > 155) {
    alert('Message is too long! Only 155 characters per message');
    return null;
  }
  const t_diff = (new Date()).getTime() - last_msg_time.getTime();
  if (t_diff < 3000) {
    alert('Please wait 3 seconds between messages.');
    return null;
  }
  last_msg_time = new Date();
  const msg = {};
  msg.text = text;
  msg.responses = [];
  msg.topic = '@tg';
  msg.user_name = user_name;
  input.value = '';
  return JSON.stringify(msg);
}

function scrollToBottom() {
  g_is_scrolling = true;
  const msgs_div = document.getElementById('msgs');
  msgs_div.scrollTop = msgs_div.scrollHeight;
  const scroll_down = document.querySelector('.scroll_down');
  if (scroll_down) {
    scroll_down.parentNode.removeChild(scroll_down);
  }
  const input = document.getElementById('input');
  input.focus();
}

function showScrollDown() {
  if (document.querySelector('.scroll_down')) {
    return;
  }
  const scroll_down = elem('div', 'scroll_down');
  scroll_down.textContent = 'scroll down';
  scroll_down.addEventListener('click', function() {
    scrollToBottom();
  });
  document.body.appendChild(scroll_down);
}

function chatSetup() {
  localStorage.setItem('challenge', 'completed');
  if (localStorage.getItem('name') !== 'null') {
    user_name = localStorage.getItem('name');
  } else {
    user_name = prompt('Name? Leave empty for \'Anonymous\'');
    user_name = user_name ? user_name : 'Anonymous';
    localStorage.setItem('name', user_name);
  }

  const socket = new WebSocket('wss://'+window.location.hostname);
  socket.onopen = function() {
    console.log('connected!');
  };
  socket.onclose = function() {
    if (socket.readyState == WebSocket.CLOSED ||
                socket.readyState == WebSocket.CLOSING) {
      location.reload();
    }
  };
  socket.onerror = function() {
    if (socket.readyState == WebSocket.CLOSED ||
                socket.readyState == WebSocket.CLOSING) {
      location.reload();
    }
  };


  socket.onmessage = function(event) {
    renderMessage(event.data);
  };

  const input = document.getElementById('input');
  input.addEventListener('keyup', function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
      const valid_msg = validateMessage( input );
      if (valid_msg) {
        socket.send(valid_msg);
      }
    }
  });
  input.focus();
  document.body.addEventListener('touchmove', function(e) {
    e.preventDefault();
  });
  const msgs_div = document.getElementById('msgs');
  msgs_div.addEventListener('scroll', function() {
    max_scroll = Math.max(max_scroll, msgs_div.scrollTop);
    if (msgs_div.scrollTop >= (max_scroll - 50)) {
      g_is_scrolling = true;
      const scroll_down = document.querySelector('.scroll_down');
      if (scroll_down) {
        scroll_down.parentNode.removeChild(scroll_down);
      }
    } else {
      showScrollDown();
      g_is_scrolling = false;
    }
  });
  setTimeout(function() {
    scrollToBottom();
  }, 200);
}

function challengeQuery() {
  if (localStorage.getItem('challenge') == 'completed') {
    const elem = document.querySelector('#challenge');
    elem.parentNode.removeChild(elem);
    chatSetup();
    return;
  }
  const input = document.getElementById('challenge_input');
  input.addEventListener('keyup', function(event) {
    event.preventDefault();
    if (input.value.toLowerCase().includes('zyzz') ||
                input.value.toLowerCase().includes('zeez')
    ) {
      const elem = document.querySelector('#challenge');
      elem.parentNode.removeChild(elem);
      chatSetup();
    }
  });
  input.focus();
}

window.addEventListener('load', challengeQuery);
window.addEventListener('focus', function() {
  document.title = 'chat.fit';
  unread_msgs = 0;
  tab_focused = true;
});
window.addEventListener('blur', function() {
  tab_focused = false;
});


